package fr.enneade.deta.senpai.factory

import java.util.Properties

import scala.io.Source

object PropertiesFactory {

  def apply() = {
    val url = getClass.getResource("/application.properties")
    val properties: Properties = new Properties()
    properties.load(Source.fromURL(url).bufferedReader())
    properties
  }


}
