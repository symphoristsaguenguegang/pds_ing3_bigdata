package fr.enneade.deta.senpai.main

import java.util.Properties
import java.util.logging.{Level, Logger}

import com.mongodb.spark.MongoSpark
import fr.enneade.deta.senpai.factory.PropertiesFactory
import org.apache.commons.collections.list.TypedList
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, current_timestamp, datediff, floor, months_between, round, year, when}

import scala.sys.exit
import org.apache.spark.sql.functions.typedLit

object DetaSenpaiMain {

  val logger = Logger.getLogger(DetaSenpaiMain.getClass.getName)

  def main(args: Array[String]) = {

    logger.setLevel(Level.INFO)
   /* if(args == null || args.isEmpty){
      logger.severe("the file path must be provided")
      exit(1)
    }*/

    try {
      implicit val properties: Properties = PropertiesFactory()

      val location = properties.getProperty("deta.senpai.mongo.location")
      val database = properties.getProperty("deta.senpai.mongo.database")
      val collection = properties.getProperty("deta.senpai.mongo.collection")
      val mongoUri = s"mongodb://${location}/${database}.${collection}"

      implicit val spark: SparkSession = SparkSession.builder()
        .master("local")
        .appName(properties.getProperty("deta.senpai.application.name"))
        .config("spark.mongodb.input.uri", mongoUri)
        .config("spark.mongodb.output.uri", mongoUri)
        .getOrCreate()

      val filePath = "C:\\Users\\etudiant\\Documents\\ING3\\PDS\\PDSJ5\\chicago\\*.csv"
      //val filePath = "/pds/BigData/chicago/*.csv"
      val chicago = spark.read.option("header", true).option("inferSchema", true).option("delimiter", ",").csv(filePath)
      logger.info(s"File(s) retrieved from $filePath")

      /* val residentsWithAge = residents.withColumn("age",
        floor(months_between(current_timestamp(),col("birthDate"), true).divide(12)))*/

      // Columns renaming

      val chicagoReduce = chicago.select("Data Stream ID", "Measurement Time", "Measurement Value")
      val chicago2 = chicagoReduce.withColumn("Message", typedLit("panne"))
                    .withColumn("Severity", typedLit("1"))
      //scala.util.Random.nextInt(5)

      chicago2.limit(50).show()

      val map : Map(Int, Float) = Map(33264->100, 33265->0.28, 33303->1020)
      chicago2.foreach(row => if(row.getAs[Float]("Measurement Value") > map(row.getAs[Int]("Data Stream ID")))
      {
        row("Severity") = 5
      })
      chicago2.limit(50).show()

      //val chicago3 = chicago2.withColumn("Severity", when(col("Measurement Value") > map(col("Data Stream ID")), 5).otherwise(1))
     // chicago3.limit(50).show()

      //chicagoReduce.limit(100).show()

      //MongoSpark.save(chicago3)

    } catch {
      case e:Exception => {
        logger.severe(e.getMessage)
        e.printStackTrace()
      }
    }
  }


}
